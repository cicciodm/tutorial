﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Main : MonoBehaviour {

	public Vector3 menuCoords;
	public HandController controller;

	// Use this for initialization
	void Start () {

		Object[] models = Resources.LoadAll (ResourceNames.MODELS_FOLDER);
		List<string> fileNames = new List<string> ();
		
		foreach (Object m in models) {
			fileNames.Add(m.name);

			GameObject go = m as GameObject;
			
			foreach(Transform child in go.transform) {
				GameObject obj = child.gameObject;
				
				if(obj.GetComponent<PrintOnTouch>() == null){
					obj.AddComponent<PrintOnTouch>();
				}
			}
		}
		
		//This should garbage collect all the models, we don't need them
		models = null;

		PointableMenu.createPointableMenu(gameObject, fileNames, menuCoords, controller);
		TouchMain.createTouchMain(gameObject, controller);
	}
}
