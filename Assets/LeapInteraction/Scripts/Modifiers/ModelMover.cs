using UnityEngine;
using System.Collections;
using System;
using Leap;

public class ModelMover : Mover {

	private HandController controller;
	private GameObject target;
	private Vector3 cameraPosition;
	
	private int FRAME_WINDOW = 20;
	
	private Vector3MovingAverager avg;

	private Vector3 handInitialPosition;
	private Vector3 objInitialPosition;
	private Vector3 objInitialScale;
	private bool startPositionSet;		

	private float MAX_SCAL_FACTOR = 1;
	private float MIN_SCAL_FACTOR = 5;

	private float MAX_DISTANCE = 40;
	private float MIN_DISTANCE = 20;

	private float movementScalingFactor;


	public ModelMover(GameObject target, HandController controller, Vector3 cameraPosition) {
		this.target = target;
		this.controller = controller;
		this.cameraPosition = cameraPosition;
		this.movementScalingFactor = MAX_SCAL_FACTOR;

		this.handInitialPosition = Vector3.zero;
		this.objInitialPosition = target.transform.position;

		this.avg = new Vector3MovingAverager (FRAME_WINDOW);
	}

	public void moveModel(EnhancedHand hand) {
	
		Vector3 position = hand.getPalmPosition (controller);
			
		avg.updateAverage (position);

		if (avg.isStableAverage ()) {

			if(!startPositionSet) {
				handInitialPosition = hand.getPalmPosition(controller);
				objInitialPosition = target.transform.position;
				objInitialScale = target.transform.localScale;
			}

			startPositionSet = true;

			Vector3 movingAvg = avg.getAverage ();

			target.transform.position = objInitialPosition + (movingAvg - handInitialPosition) * 3;
		}
	}

	public void stopMove() {
		float modelDistance = (target.transform.position - cameraPosition).magnitude;
//		movementScalingFactor = calculateScalingFactor(modelDistance);
		startPositionSet = false;
		avg.resetMeasurements();
	}

	private float calculateScalingFactor(float distance) {
		float distanceRange = MAX_DISTANCE - MIN_DISTANCE;
		float scaleRange = MAX_SCAL_FACTOR - MIN_SCAL_FACTOR;

		return (((distance - MIN_DISTANCE) * scaleRange) / distanceRange) + MIN_SCAL_FACTOR;
	}

	private Vector3 vectorClamp (Vector3 v, float min, float max) {
		float x = Mathf.Clamp (v.x, min, max);
		float y = Mathf.Clamp (v.y, min, max);
		float z = Mathf.Clamp (v.z, min, max);
		return new Vector3 (x, y, z);
	}	
}
