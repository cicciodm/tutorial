using UnityEngine;
using System.Collections;
using System;
using Leap;

public class ModelRotator : Rotator {

	//TODO:change to private
	private HandController controller;
	private Rigidbody rb;

	private int FRAME_WINDOW = 30;

	private bool isVelocityIncreasing;
	private bool isStill;

	private Frame rotationInitialFrame;
	private bool wasClosed;

	private float previousRotationZ;

	private const float VELOCITY_THRESH = 30f;
	private const float ROTATION_THRESH = 0.01f;
	private const float FACTOR = 45;
	private const float FORCE_THRESH = 0.7f;
	private const float DRAG_MOVE = 0.5f;
	private const float DRAG_STOP = 5f;

	Vector3MovingAverager avg;

	public ModelRotator(GameObject target, HandController controller) {
		this.controller = controller;
		this.avg = new Vector3MovingAverager(FRAME_WINDOW);

		rb = target.AddComponent<Rigidbody> ();
		rb.useGravity = false;

	}
	
	public void rotateModel(EnhancedHand hand) {
	
		Vector3 velocity = hand.getPalmVelocity (controller);

		if (wasClosed) {
			rotationInitialFrame = hand.getLeapHand().Frame;
		}

		wasClosed = false;

		Vector3 smoothedVelocity = smoothWithThreshold (velocity, VELOCITY_THRESH);

		calculateNewAverage (smoothedVelocity);

		Vector3 currAverage = avg.getAverage() / FACTOR;

		float rotationAroundZ = hand.getLeapHand().RotationAngle(rotationInitialFrame, Vector.ZAxis);
		float rotationDifference = previousRotationZ - rotationAroundZ;
		previousRotationZ = rotationAroundZ;

		Vector3 torque = new Vector3 (currAverage.y, -currAverage.x, 0);

		if (isVelocityIncreasing || shouldRotateZ(rotationDifference)) {
			torque = smoothWithThreshold(torque, FORCE_THRESH);

			setDrag(DRAG_MOVE);
			rb.AddTorque (torque);
		} else {
			if(isStill){
				setDrag(DRAG_STOP);
			}
		}
	}

	public void stopRotation() {
		setDrag(DRAG_STOP);
		avg.resetMeasurements ();
		previousRotationZ = 0;
		wasClosed = true;
	}

	private void calculateNewAverage(Vector3 velocity) {
		Vector3 oldAverage = avg.getAverage();
		avg.updateAverage (velocity);
		
		isVelocityIncreasing = avg.getAverage().magnitude > oldAverage.magnitude;
		isStill = velocity.magnitude < 20;
	}

	private Vector3 smoothWithThreshold(Vector3 vector, float threshold) {
		float newX = Math.Abs(vector.x) > threshold ? vector.x : 0;
		float newY = Math.Abs(vector.y) > threshold ? vector.y : 0;
		float newZ = Math.Abs(vector.z) > threshold ? vector.z : 0;
		return new Vector3 (newX, newY, newZ);
	}

	private bool shouldRotateZ(float val){
		return Math.Abs (val) > ROTATION_THRESH;
	}

	private void setDrag(float drag) {
		rb.angularDrag = drag;
	}
}
