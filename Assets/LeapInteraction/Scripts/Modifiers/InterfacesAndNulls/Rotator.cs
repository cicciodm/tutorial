using UnityEngine;
using System.Collections;
using System;
using Leap;

public interface Rotator {

	void rotateModel(EnhancedHand hand);
	void stopRotation();

}

