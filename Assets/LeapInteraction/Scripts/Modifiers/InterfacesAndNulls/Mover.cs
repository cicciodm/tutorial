﻿using UnityEngine;
using System.Collections;

public interface Mover {

	void moveModel(EnhancedHand hand);
	void stopMove();
}
