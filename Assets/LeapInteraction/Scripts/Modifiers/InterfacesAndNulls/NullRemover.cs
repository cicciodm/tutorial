﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;

public class NullRemover : Remover {

	public GestureID changeIfSwipe(Frame frame){
		return GestureID.NONE;
	}

	public GestureID changeLayers(Vector3 dir){
		return GestureID.NONE;
	}

	public void toggleDRE(List<GameObject> newObjectsToHide){
	
	}
}
