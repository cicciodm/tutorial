﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;

public interface Remover {

	GestureID changeIfSwipe(Frame frame);
	GestureID changeLayers(Vector3 dir);
	void toggleDRE(List<GameObject> newObjectsToHide);

}