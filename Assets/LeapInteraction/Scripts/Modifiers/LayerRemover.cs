﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;

public class ModelLayerRemover : Remover {

	private HandController controller;

	private int indexToHide = -1;
	private List<GameObject> objectsToHide;
	private int i = 0;
	private HashSet<int> seenSwipeIds;
	private bool stillSwiping;

	// Use this for initialization
	public ModelLayerRemover (HandController controller, List<GameObject> objectsToHide) {
		this.controller = controller;
		this.objectsToHide = objectsToHide;
		this.seenSwipeIds = new HashSet<int> ();

		GameObject firstLayer = this.objectsToHide [0];

	}

	public GestureID changeIfSwipe(Frame frame) {
		//Retrieve the direction of the frame swipe gesture, if any

		GestureList gestures = frame.Gestures ();

		if (gestures.Count == 0) {
			stillSwiping = false;
			return GestureID.NONE;
		}

		List<SwipeGesture> swipes = filterSwipes (gestures);
		if (swipes.Count > 0) {
			stillSwiping = true;
			Vector leapDir = swipes [0].Direction;
			return changeLayers (Converter.LeapToWorldDirection (controller, leapDir));
		} else {
			return GestureID.NONE;
		}
	}


	public GestureID changeLayers(Vector3 dir) {
		GameObject toToggle = null;
		int currentVisibleIndex = -1;
		GestureID g = GestureID.NONE;

		if (dir.x > 0) {
			if(indexToHide < objectsToHide.Count - 1) {
				indexToHide++;
				toToggle = objectsToHide[indexToHide];
				currentVisibleIndex = indexToHide + 1;
				g = GestureID.SWIPE_RIGHT;
			}
		} else if (dir.x < 0) {
			if(indexToHide >= 0) {
				toToggle = objectsToHide[indexToHide];
				currentVisibleIndex = indexToHide;
				indexToHide--;
				g = GestureID.SWIPE_LEFT;
			}
		} else {
			return GestureID.NONE;
		}
		toggleRenderers(toToggle);
//		if (currentVisibleIndex >= 0 && currentVisibleIndex < objectsToHide.Count) {
//			GameObject currentVisible = objectsToHide[currentVisibleIndex];
//		}
		return g;
	}

	public void toggleDRE(List<GameObject> newObjectsToHide) {
		objectsToHide = newObjectsToHide;
		GameObject skin = objectsToHide [0];

		if (indexToHide != -1) {
			toggleRenderers (skin);
		} 
	}

	private void toggleRenderers(GameObject obj) {
		if (obj != null) {
			Renderer[] renderers = obj.GetComponentsInChildren<Renderer> ();
			foreach (Renderer r in renderers) {
				r.enabled = !r.enabled;
			}
		}
	}

	private List<SwipeGesture> filterSwipes(GestureList list) {
		//Should add something to only get swipes.
		//Not needed for now cause swipes are the only gestures recognised.

		//Ignore all previously seen swipes

		List<SwipeGesture> final = new List<SwipeGesture> ();

		foreach (Gesture g in list) {
			SwipeGesture swipe = new SwipeGesture(g);
			if(!seenSwipeIds.Contains(swipe.Id) & !stillSwiping) {
				final.Add(swipe);
				seenSwipeIds.Add(swipe.Id);
			} 
		}
		return final;
	}
}
