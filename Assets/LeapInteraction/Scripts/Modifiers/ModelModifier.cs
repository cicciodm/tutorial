using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;

public class ModelModifier : MonoBehaviour {

	private Controller leap;
	private HandController controller;

	private Dictionary<Finger.FingerType, GameObject> gestureFingers;

	private const int FINGERS_MOVE = 3;
	private const int FINGERS_SWIPE = 2;

	//Modifier Objects
	private ModelRotator rotator;
	private ModelMover mover;
	private ModelLayerRemover remover;

	private FloatingText ft;

	private MenuManager menuManager;

	// Use this for initialization
	void Start () {
		leap = new Controller ();
	}

	public static ModelModifier createModelModifier(GameObject target, 
	                                          		HandController controller,
	                                                MenuManager menuManager) {

		ModelModifier mov = target.AddComponent<ModelModifier> ();
		mov.controller = controller;
		mov.menuManager = menuManager;

		mov.rotator = new ModelRotator (target, controller);

		Vector3 cameraPos = GameObject.FindGameObjectWithTag ("MainCamera").transform.position;
		mov.mover = new ModelMover (target, controller, cameraPos);


		List<GameObject> layers = getObjectsToHide (target);
		mov.remover = new ModelLayerRemover (controller, layers);

		mov.gestureFingers = new Dictionary<Finger.FingerType, GameObject> ();
		mov.setUpGestureFingers ();

		FloatingText.FloatingTextBuilder ftBuilder = 
			new FloatingText.FloatingTextBuilder ();
		
		mov.ft = ftBuilder.lookingTowards(cameraPos).buildFloatingText ();

		return mov;
	}
	
	// Update is called once per frame
	void Update () {
		EnhancedHand hand = HandManager.getLeftEnhancedHand (leap);
		FingerList fingers = hand.getExtendedFingers ();
		int extended = fingers.Count;

		bool gestureRecog = false;

		bool isRotating = false;
		bool isMoving = false;

		string state = "";
		string desc = "";

		bool isDRELoaded = menuManager.getDREStatus();

		if (hand.isOpen () && !isDRELoaded) {
			gestureRecog = true;
			isRotating = true;

			state = Strings.OPEN_STATE;
			desc = Strings.OPEN_DESC;

			rotator.rotateModel (hand);
		} else if (extended == FINGERS_MOVE && !isDRELoaded) {
			gestureRecog = true;
			isMoving = true;

			state = Strings.MOVE_STATE;
			desc = Strings.MOVE_DESC;

			mover.moveModel (hand);
		} else if (extended == FINGERS_SWIPE) {
			gestureRecog = true;

			state = Strings.SWIPE_STATE;
			desc = Strings.SWIPE_DESC;

			remover.changeIfSwipe (leap.Frame ());
		} else {
			ft.resetFloatingText();
			deleteAllPointers();
		}

		if (gestureRecog) {
			updatePointers (fingers);
			updateGestureText(state, desc, fingers, hand.getLeapHand().IsLeft);
		}

		//Cool down
		if (!isRotating) {
			rotator.stopRotation();
		}

		if (!isMoving) {
			mover.stopMove();
		}
	}

	public void toggleDRE(GameObject patient) {
		remover.toggleDRE(getObjectsToHide(patient));
	}

	private void updateGestureText(string state, string desc, FingerList fingers, bool isLeft) {
		Finger centreMost = !isLeft ? fingers.Rightmost : fingers.Leftmost;
		Vector3 fingerTip = Converter.LeapToWorldPoint (controller, centreMost.TipPosition);

		ft.updateFloatingText (fingerTip, centreMost.Id, state, desc, isLeft);
	}

	private void updatePointers(FingerList fingers) {
		List<Finger.FingerType> keys = new List<Finger.FingerType> (gestureFingers.Keys);

		foreach (Finger.FingerType type in keys) {
			FingerList ofType = fingers.FingerType (type);
			
			if(!ofType.IsEmpty) {
				updatePointerForFingerType(type, ofType[0]);
			} else {
				safelyDeletePointer(type);
			}
		}
	}

	private void updatePointerForFingerType(Finger.FingerType type, Finger f) {
		GameObject pointer; 
		gestureFingers.TryGetValue (type, out pointer);

		if (pointer == null) {
			GameObject newPointer = initPointer();
			gestureFingers[type] = newPointer;
			pointer = newPointer;
		}

		setPointerPosition (f, pointer);
	}

	private void deleteAllPointers() {
		List<Finger.FingerType> keys = new List<Finger.FingerType> (gestureFingers.Keys);
		foreach (Finger.FingerType type in keys) {
			safelyDeletePointer(type);
		}
	}

	private void safelyDeletePointer(Finger.FingerType type) {
		GameObject pointer; 
		gestureFingers.TryGetValue (type, out pointer);
		
		if (pointer != null) {
			GameObject.Destroy(pointer);
			gestureFingers[type] = null;
		}
	}

	private GameObject initPointer() {
		GameObject p = Resources.Load (ResourceNames.MENU_POINTER) as GameObject;
		GameObject copy = GameObject.Instantiate (p) as GameObject;
		return copy;
	}


	void setPointerPosition (Finger f, GameObject pointer) {
		Vector3 pos = Converter.LeapToWorldPoint (controller, f.TipPosition);
		pointer.transform.position = pos;
	}

	private void setUpGestureFingers() {
		gestureFingers.Add (Finger.FingerType.TYPE_THUMB, null);
		gestureFingers.Add (Finger.FingerType.TYPE_INDEX, null);
		gestureFingers.Add (Finger.FingerType.TYPE_MIDDLE, null);
		gestureFingers.Add (Finger.FingerType.TYPE_RING, null);
		gestureFingers.Add (Finger.FingerType.TYPE_PINKY, null);
	}

	private static List<GameObject> getObjectsToHide(GameObject target) {
		List<GameObject> list = new List<GameObject>();
		
		//Based precisely on patient object
		GameObject skin = target.transform.GetChild(0).gameObject;

		GameObject parts = target.transform.GetChild (1).gameObject;
		GameObject anatomy = parts.transform.GetChild (0).gameObject;

		GameObject repro = anatomy.transform.GetChild (0).gameObject;
		GameObject skeleton = anatomy.transform.GetChild (1).gameObject;

		list.Add (skin);
		list.Add (skeleton);
		list.Add (repro);
		
		return list;
	}
}
