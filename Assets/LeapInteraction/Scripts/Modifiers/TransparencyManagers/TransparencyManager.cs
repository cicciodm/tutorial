﻿using UnityEngine;
using System.Collections;

public abstract class TransparencyManager : MonoBehaviour{

	public abstract void setTransparency(float value);
	public abstract float getTransparency();

}