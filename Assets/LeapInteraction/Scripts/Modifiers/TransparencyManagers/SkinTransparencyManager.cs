﻿using UnityEngine;
using System.Collections;

public class SkinTransparencyManager : TransparencyManager {

	override public void setTransparency(float value) {
		Renderer[] rs = gameObject.GetComponentsInChildren<Renderer> ();

		Color col;
		foreach (Renderer r in rs) {
			if(value == 1) {
				setModeOpaque(r.material);
			} else {
				setModeTransparent(r.material);
				col = r.material.color;
				col.a = value;
				r.material.color = col;
			}
		}
	}

	override public float getTransparency() {
		Renderer[] rs = gameObject.GetComponentsInChildren<Renderer> ();
		return rs[0].material.color.a;
	}

	private void setModeTransparent(Material material) {
		material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
		material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
		material.SetInt("_ZWrite", 0);
		material.DisableKeyword("_ALPHATEST_ON");
		material.DisableKeyword("_ALPHABLEND_ON");
		material.EnableKeyword("_ALPHAPREMULTIPLY_ON");
		material.renderQueue = 3000;
	}

	private void setModeOpaque(Material material) {
		material.SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
		material.SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
		material.SetInt("_ZWrite", 1);
		material.DisableKeyword("_ALPHATEST_ON");
		material.DisableKeyword("_ALPHABLEND_ON");
		material.DisableKeyword("_ALPHAPREMULTIPLY_ON");
		material.renderQueue = -1;
	}
}