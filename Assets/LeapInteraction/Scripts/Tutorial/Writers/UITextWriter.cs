﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public abstract class UITextWriter : TextWriter {

	protected Text textElem;

	public UITextWriter() {
		setUpTextField ();
	}

	protected abstract void setUpTextField ();

	public void writeText(string text) {
		textElem.text = text;		
	}

}
