﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameWorldTextWriter : UITextWriter {

	protected override void setUpTextField () {
		GameObject box = GameObject.FindGameObjectWithTag (TutorialTags.INFO_BOX);
		textElem = box.GetComponent<Text> ();
		textElem.text = "";
	}
}
