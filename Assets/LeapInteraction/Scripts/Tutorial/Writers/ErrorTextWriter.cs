﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ErrorTextWriter : UITextWriter {
	
	protected override void setUpTextField () {
		GameObject box = GameObject.FindGameObjectWithTag (TutorialTags.ERROR_BOX);
		textElem = box.GetComponent<Text> ();
		textElem.text = "";
	}
}
