﻿using UnityEngine;
using System.Collections;

public class DebugTextWriter : TextWriter {

	public void writeText(string text) {
		Debug.Log ("The following text will be written on screen: " + text);
	}
}
