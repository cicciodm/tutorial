﻿using UnityEngine;
using System.Collections;

public class ErrorDebugTextWriter : TextWriter {

	public void writeText(string message) {
		Debug.Log ("This is an Error Message: " + message);
	}
}
