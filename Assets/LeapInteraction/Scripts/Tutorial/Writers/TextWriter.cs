﻿using UnityEngine;
using System.Collections;

public interface TextWriter {

	void writeText(string text);
}
