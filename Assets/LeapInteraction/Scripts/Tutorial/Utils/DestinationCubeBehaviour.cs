﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class DestinationCubeBehaviour : MonoBehaviour {

	private GestureCompletionListener listener;
	private string s;
	public static DestinationCubeBehaviour createDestinationBehaviour(GameObject target,
	                                                                  GestureCompletionListener listener) {
		DestinationCubeBehaviour dest = target.AddComponent<DestinationCubeBehaviour> ();
		dest.listener = listener;
		dest.s = "sticazzi";
		Debug.Log (listener);

		return dest;
	}

	// Use this for initialization
	void Start () {
		scaleUp ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void scaleUp() {
		transform.DOScale (2.5f, 0.9f).OnComplete(scaleDown);
	}

	public void scaleDown() {
		transform.DOScale (2, 0.9f).OnComplete(scaleUp);
	}

	public void OnTriggerEnter(Collider other) {
		if(other.tag == TutorialTags.MOVING_CUBE) {
			Debug.Log(s);
			this.listener.gesturePerformed(GestureID.MOVE);
			Destroy(gameObject);
		}
	}
}

