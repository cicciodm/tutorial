﻿using UnityEngine;
using System.Collections;

public class TouchResponder : MonoBehaviour {

	private RespondToButtonPress responder;

	public static TouchResponder createTouchResponder (GameObject target, 
	                                                   RespondToButtonPress responder) {

		TouchResponder r = target.AddComponent<TouchResponder> ();
		r.responder = responder;

		return r;
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == TutorialTags.INDEX_COLLIDER) {
			responder();
			Destroy(gameObject);
		}
	}
}
