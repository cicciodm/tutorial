﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class RotatingCubeBehaviour : MonoBehaviour {

	private Vector3 direction;
	private GameObject targetQuad;
	private GestureCompletionListener listener;

	public static RotatingCubeBehaviour createRotatingCube(GameObject target,
	                                                       CubeColours colour,
	                                                       GameObject targetQuad,
	                                                       GestureCompletionListener listener) {

		RotatingCubeBehaviour rot = target.AddComponent<RotatingCubeBehaviour> ();

		rot.direction = getDirectionFromCol (colour);

		switch (colour) {
			case CubeColours.GREEN: rot.direction = Vector3.up; break;
			case CubeColours.WHITE: rot.direction = Vector3.right; break;
			case CubeColours.RED: rot.direction = Vector3.back; break;
			case CubeColours.YELLOW: rot.direction = Vector3.left; break;
			case CubeColours.BLUE: rot.direction = Vector3.right; break;
			case CubeColours.ORANGE: rot.direction = Vector3.forward; break;
		}

		rot.targetQuad = targetQuad;
		rot.listener = listener;

		return rot;

	}

	public void setColourToReach(CubeColours col) {
		direction = getDirectionFromCol (col);
	}

	// Use this for initialization
	void Start () {
//		rotate ();
	}


	// Update is called once per frame
	void Update () {
		RaycastHit hit;
		Vector3 dir = transform.TransformDirection (direction);

		Debug.DrawRay (transform.position, dir * 20, Color.red);

		if (Physics.Raycast (transform.position, dir, out hit)) {
			if(hit.collider.tag == TutorialTags.TARGET_QUAD) {
				Debug.Log("Hit");
				listener.gesturePerformed(GestureID.ROTATE);	
			}
		}

	}

	private static Vector3 getDirectionFromCol(CubeColours colour) {
		switch (colour) {
		case CubeColours.GREEN:  return Vector3.up; 
		case CubeColours.WHITE:  return Vector3.right; 
		case CubeColours.RED: 	 return Vector3.back; 
		case CubeColours.YELLOW: return Vector3.left; 
		case CubeColours.BLUE: 	 return Vector3.right;
		case CubeColours.ORANGE: return Vector3.forward;
		default: return Vector3.zero; 
		}
	}

	private void rotate() {
		transform.DORotate (new Vector3(359, 359, 359), 10, RotateMode.FastBeyond360).OnComplete(rotate);
	}
}
