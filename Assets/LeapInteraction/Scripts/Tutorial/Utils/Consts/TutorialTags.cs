﻿using UnityEngine;
using System.Collections;

public class TutorialTags {

	public const string CONTROLLER = "controller";
	public const string INFO_BOX = "info_box";
	public const string ERROR_BOX = "error_box";
	public const string INDEX_COLLIDER = "index_collider";
	public const string MOVING_CUBE = "moving_cube";
	public const string TARGET_QUAD = "target_quad";
}
