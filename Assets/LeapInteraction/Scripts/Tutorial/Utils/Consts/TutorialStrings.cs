﻿using UnityEngine;
using System.Collections;

public class TutorialStrings {

	public const string DUMMY = "This is a Dummy.";

	public const string WELCOME = "Welcome. Here You will learn how to control the simulation." +
									"\nPress any key to continue.";

	public const string POINT_TEACH = "The first gesture is Pointing. It is used to interact with menus." +
										"\nTo point, extend your index finger.";

	public const string POINT_TAUGHT = "Well done, you learned how to point!" +
										"\nPress Space to continue, or R to retry.";

	public const string SWIPE_TEACH = "Swiping with two fingers is used to add/remove layers." +
										"\nExtend your Index and Middle fingers.";
	public const string SWIPE_TAUGHT = "Well done, you learned the swiping hand pose!" +
										"\nPress Space to continue, or R to retry.";

	public const string MOVE_TEACH = "Moving the model is performed by extending three fingers." +
										"\nExtend your Thumb, Index and Middle fingers.";
	public const string MOVE_TAUGHT = "Well done, you learned the moving hand pose!" +
										"\nPress Space to continue, or R to retry.";

	public const string ROTATE_TEACH = "Moving the model is performed by extending all five fingers." +
										"\nOpen your hand now.";
	public const string ROTATE_TAUGHT = "Well done, you learned the moving hand pose!" +
										"\nPress Space to continue, or R to retry.";

	public const string POSES_TAUGHT = "Well Done, you learned all the Hand Poses!" +
										"\nPress Space to continue.";

	public const string GESTURE_TEACH_START = "You will now learn how to use these poses to control objects in the scene" +
										"\nPress Space to continue.";

	public const string G_POINT_TEACH = "Will teach how to point";
	public const string G_POINT_TAUGHT = "TAUGHT YOU HOW TO POINT";

	public const string G_SWIPE_TEACH = "Will teach how to swipe";
	public const string LAYERS_REMOVED = "Layers have been removed, now add them back swiping in the opposite direction";
	public const string G_SWIPE_TAUGHT = "TAUGHT YOU HOW TO SWIPE";

	public const string G_MOVE_TEACH = "Will teach how to move";
	public const string SINGLE_MOVE_COMPLETE = "Move complete, try once more!";
	public const string G_MOVE_TAUGHT = "TAUGHT YOU HOW TO move";

	public const string G_ROTATE_TEACH = "Will teach how to ROTATE";

	public const string ROTATE_LEFT_COMPLETE = "Move1 complete, try once more!";
	public const string ROTATE_RIGHT_COMPLETE = "Move2 complete, try once more!";
	public const string ROTATE_UP_COMPLETE = "Move3 complete, try once more!";
	public const string ROTATE_DOWN_COMPLETE = "Move4 complete, try once more!";

	public const string G_ROTATE_TAUGHT = "TAUGHT YOU HOW TO ROTATE";

}
