using UnityEngine;
using System.Collections;

public enum GestureID {
	NONE,
	SWIPE_LEFT,
	SWIPE_RIGHT,
	MOVE,
	ROTATE
}

