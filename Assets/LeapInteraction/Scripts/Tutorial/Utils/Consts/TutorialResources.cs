﻿using UnityEngine;
using System.Collections;

public class TutorialResources {

	public const string PROGRESS_QUAD = "TutorialAssets/ProgressQuad";
	public const string MULTISPHERE = "TutorialAssets/MultiSphere";
	public const string BUTTON = "MenuAssets/TextMenuMain";
	public const string MOVING_CUBE = "TutorialAssets/MovingCube";
	public const string DESTINATION_CUBE = "TutorialAssets/DestinationCube";
	public const string ROTATING_CUBE = "TutorialAssets/RotatingCube";
	public const string TARGET_QUAD = "TutorialAssets/TargetQuad";
}
