﻿using UnityEngine;
using System;
using System.Collections;

public enum ModifierFlags {
	Default = 0,
	SWIPE = 1,
	MOVE = 2,
	ROTATE = 4,
	ALL = 8
}
