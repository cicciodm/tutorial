﻿using UnityEngine;
using System.Collections;

public enum TeachingStatus {
	STATUS_POINT,
	STATUS_SWIPE,
	STATUS_MOVE,
	STATUS_ROTATE
}
