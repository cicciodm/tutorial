﻿using UnityEngine;
using System.Collections;

public enum CubeColours {
	NONE,
	WHITE,
	BLUE,
	RED,
	YELLOW,
	GREEN,
	ORANGE
}
