﻿using UnityEngine;
using System.Collections;

public class ButtonCreator {

	private static GameObject buttonPrefab = Resources.Load(TutorialResources.BUTTON) as GameObject;

	public static void createButton(RespondToButtonPress res, Vector3 pos) {

		GameObject button = GameObject.Instantiate (buttonPrefab);
		button.transform.position = pos;

		TouchResponder.createTouchResponder (button, res);
	}
}
