using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;

public class TutorialModelModifier : MonoBehaviour {
	
	private Controller leap;
		
	private const int FINGERS_MOVE = 3;
	private const int FINGERS_SWIPE = 2;
	
	//Modifier Objects
	private Rotator rotator;
	private Mover mover;
	private Remover remover;

//	private ModifierFlags flag;

	private GestureCompletionListener completionListener;
	
	// Use this for initialization
	void Start () {
		leap = new Controller ();
	}
	
	public static TutorialModelModifier createModelModifier(GameObject target, 
	                                                		HandController controller,
	                                                		ModifierFlags flag) {


		TutorialModelModifier mov = target.AddComponent<TutorialModelModifier> ();

//		mov.flag = flag;
	
		if (hasFlag (flag, ModifierFlags.ROTATE)) {
			mov.rotator = new ModelRotator (target, controller);
		} else {
			mov.rotator = new NullRotator();
		}


		if (hasFlag (flag, ModifierFlags.MOVE)) {
			Vector3 cameraPos = GameObject.FindGameObjectWithTag ("MainCamera").transform.position;
			mov.mover = new ModelMover (target, controller, cameraPos);
		} else {
			mov.mover = new NullMover();
		}

		if (hasFlag (flag, ModifierFlags.SWIPE)) {
			List<GameObject> layers = getObjectsToHide (target);
			mov.remover = new ModelLayerRemover (controller, layers);
		} else {
			mov.remover = new NullRemover();
		}


		mov.completionListener = new NullGestureCompletionListener ();

		return mov;
	}

	public void setCompletionListener(GestureCompletionListener listener) {
		this.completionListener = listener;
	}
	
	// Update is called once per frame
	void Update () {
		EnhancedHand hand = HandManager.getLeftEnhancedHand (leap);
		FingerList fingers = hand.getExtendedFingers ();
		int extended = fingers.Count;
		
		bool swipeRecog = false;
		
		bool isRotating = false;
		bool isMoving = false;

		GestureID gesture = GestureID.NONE;

		if (hand.isOpen ()) {
			isRotating = true;
			rotator.rotateModel (hand);
		} else if (extended == FINGERS_MOVE) {
			isMoving = true;
			mover.moveModel (hand);
		} else if (extended == FINGERS_SWIPE) {
			swipeRecog = true;
			gesture = remover.changeIfSwipe (leap.Frame ());

		} else {

		}
		
		if (swipeRecog) {
			completionListener.gesturePerformed(gesture);
		}
		
		//Cool down
		if (!isRotating) {
			rotator.stopRotation ();
		}
		
		if (!isMoving) {
			mover.stopMove ();
		}
	}

	private static List<GameObject> getObjectsToHide(GameObject target) {
		List<GameObject> list = new List<GameObject>();

		foreach(Transform t in target.transform) {
			if(t.name != "S"){
				list.Add(t.gameObject);
			}
		}

		return list;
	}

	private static bool hasFlag(ModifierFlags flag, ModifierFlags other) {
		return (flag & other) == other;
	}
}
