﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;

public delegate void RespondToInput(KeyCode key);
public delegate void RespondToTeachingCompletion();
public delegate void RespondToPoseRecognition(string text);
public delegate void RespondToGestureTaught(string text);
public delegate void RespondToButtonPress();
public delegate void RespondToGestureCompletion();

public class TutorialPerformer {
	
	private TextWriter writer;
	private HandPoseTeacher handPoseTeacher;
	private GestureTeacher gestureTeacher;

	public TutorialPerformer(Controller leap, HandController controller) {

		writer = new GameWorldTextWriter ();

		RespondToTeachingCompletion res = posesTaught;
		handPoseTeacher = new HandPoseTeacher (leap, writer, res);

		RespondToTeachingCompletion res1 = gesturesTaught;
		gestureTeacher = new GestureTeacher (leap, controller, writer, res1);
	}

	public void startTutorial() {

		writer.writeText (TutorialStrings.WELCOME);
		//TODO: change this back to respondToTutorialStart
		RespondToInput res = respondToGestureTeachingStart;

		waitForAnyInput (res);
	}

	private void respondToTutorialStart(KeyCode code) {
		//Ignore code;
		handPoseTeacher.teachHandPoses ();
	}

	private void posesTaught () {
		writer.writeText(TutorialStrings.POSES_TAUGHT);
		RespondToInput res = respondToConfirmPosesTaught;

		waitForAnyInput (res);
	}

	private void respondToConfirmPosesTaught(KeyCode code) {
		//Ignore code
		writer.writeText (TutorialStrings.GESTURE_TEACH_START);
		RespondToInput res = respondToGestureTeachingStart;

		waitForAnyInput (res);
	}

	private void respondToGestureTeachingStart(KeyCode code) {
		//Ignore code
		gestureTeacher.teachGestures ();
	}

	private void gesturesTaught() {
		writer.writeText("Gestures taught");
	}

	private void waitForAnyInput(RespondToInput res) {
		GameObject reader = new GameObject ("InputReader");
		InputReader.createInputReader (reader, res, null);
	}

}
