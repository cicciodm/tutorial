using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Leap;

public class HandPoseRecogniser : MonoBehaviour {

	private RespondToPoseRecognition responder;
	private Controller leap;
	private List<Finger.FingerType> fingerList;
	private string success;
	private TextWriter errorWriter;

	private GameObject progress;

	private int FRAME_LENGTH = 100;
	private int ERROR_FRAME_LENGTH = 200;
	private int currentFrameNum = 0;
	private int currentWrongFrames = 0;

//	private Vector3 PROG_POSITION = new Vector3(14.2f, 3.5f, 21f);
//	private Vector3 PROG_SCALE = new Vector3(5, 5, 5);

	public static HandPoseRecogniser createPoseRecogniser(GameObject target,
	                                                      RespondToPoseRecognition responder,
	                                                      Controller leap,
	                                                      List<Finger.FingerType> fingerList,
	                                                      string success) {

		HandPoseRecogniser recog = target.AddComponent<HandPoseRecogniser> ();
		recog.responder = responder;
		recog.leap = leap;
		recog.fingerList = fingerList;
		recog.success = success;
		recog.errorWriter = new ErrorTextWriter ();

		return recog;
	}

	void Start() {
		initProgressQuad ();
	}

	// Update is called once per frame
	void Update () {

		EnhancedHand hand = HandManager.getLeftEnhancedHand (leap);

		if (!hand.getLeapHand().IsValid) {
			//Only check when hand is on screen
			return;
		}

		List<Finger.FingerType> types = hand.getExtendedFingerTypes ();



		if (listsEqual(fingerList, types)) {
			currentFrameNum++;
			currentWrongFrames = 0;

			if (currentFrameNum == FRAME_LENGTH) {
				errorWriter.writeText("");
				responder(success);
				Destroy(gameObject);
			}
		
		} else {
			currentWrongFrames++;

			if(currentWrongFrames >= ERROR_FRAME_LENGTH) {
				errorWriter.writeText("You are using the wrong fingers! Move your hand around.");
				currentWrongFrames = 0;
			}

			currentFrameNum = 0;
		}

		setProgress((float)currentFrameNum / (float)FRAME_LENGTH);

	}

	public bool listsEqual<T>(IEnumerable<T> list1, IEnumerable<T> list2) {

		var cnt = new Dictionary<T, int>();
		foreach (T s in list1) {
			if (cnt.ContainsKey(s)) {
				cnt[s]++;
			} else {
				cnt.Add(s, 1);
			}
		}
		foreach (T s in list2) {
			if (cnt.ContainsKey(s)) {
				cnt[s]--;
			} else {
				return false;
			}
		}
		return cnt.Values.All(c => c == 0);
	}

	private void initProgressQuad () {
		GameObject prefab = Resources.Load (TutorialResources.PROGRESS_QUAD) as GameObject;
		progress = GameObject.Instantiate (prefab);
		progress.transform.parent = this.transform;
	}

	private void setProgress(float p) {
		Renderer r = progress.GetComponent<Renderer> ();
		Color c = r.material.color;
		c.a = p;
		r.material.color = c;
	}
}
