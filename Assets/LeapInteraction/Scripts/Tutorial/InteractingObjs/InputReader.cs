﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class InputReader : MonoBehaviour {

	private RespondToInput responder;
	private List<KeyCode> keys;
	private KeyCode pressed;

	public static InputReader createInputReader(GameObject target, 
	                          					RespondToInput responder, 
	                          					List<KeyCode> keys) {

		InputReader reader = target.AddComponent<InputReader> ();
		reader.responder = responder;
		reader.keys = keys;

		return reader;
	}

	// Use this for initialization
	void Start () {
		StartCoroutine("startListening");
	}

	private IEnumerator startListening() {

		yield return StartCoroutine(WaitForAnyKeyDown());

		responder (pressed);
		Destroy (gameObject);
	}

	private IEnumerator WaitForAnyKeyDown() {
		while (!validKeyPressed())
			yield return null;
	}

	private bool validKeyPressed() {

		if (keys == null) {
			return Input.anyKeyDown;
		}

		bool wasPressed = false;
	
		foreach (KeyCode code in keys) {
			wasPressed |= Input.GetKeyDown(code);

			if(Input.GetKeyDown(code)) {
				pressed = code;
			}
		}
		return wasPressed;
	}

}
