﻿using UnityEngine;
using System.Collections;

public interface SingleGestureTeacher {

	void teachGesture();

}
