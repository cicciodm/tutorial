﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;

class MovingGestureTeacher : SingleGestureTeacher
{
	private RespondToGestureTaught responder;
	private string successString;

	private GameObject movingCubePrefab;
	private GameObject movingCube;

	private GameObject destinationCubePrefab;

	private HandController controller;
	private TextWriter writer;
	
	private List<GestureID> toPerform;
	private TutorialModelModifier modifier;

	private GestureCompletionListener completionListener;

	private int currRep = 0;
	private int totalReps = 3;

	private Vector2 xBounds = new Vector2 (19, -19);
	private Vector2 yBounds = new Vector2 (10, -10);
	private Vector2 zBounds = new Vector2 (19, -1);

	public MovingGestureTeacher (RespondToGestureTaught res, 
	                              string success,
	                              HandController controller,
	                              TextWriter writer) {
		this.responder = res;
		this.successString = success;
		this.movingCubePrefab = Resources.Load(TutorialResources.MOVING_CUBE) as GameObject;
		this.destinationCubePrefab = Resources.Load(TutorialResources.DESTINATION_CUBE) as GameObject;
		this.toPerform = setUpMove ();
		this.controller = controller;
		this.writer = writer;
	}
	
	public void teachGesture () {
		
		movingCube = GameObject.Instantiate (movingCubePrefab);
		movingCube.transform.position = Vector3.zero;

		modifier = TutorialModelModifier.createModelModifier (movingCube, controller, ModifierFlags.MOVE);

		RespondToGestureCompletion res = moveCompleted;

		completionListener = new TutorialGestureCompletionListener (toPerform, res);

		initDestinationCube (completionListener);
		
		modifier.setCompletionListener (completionListener);
		
	}
	
	private void moveCompleted() {
		currRep++;

		if (currRep == totalReps) {
			completedTeaching ();

		} else {

			writer.writeText (TutorialStrings.SINGLE_MOVE_COMPLETE);

			toPerform = setUpMove ();
			completionListener.setGestureList (toPerform);

			initDestinationCube (completionListener);
		}
	}

	private void initDestinationCube(GestureCompletionListener listener) {
		GameObject dest = GameObject.Instantiate (destinationCubePrefab);

		Vector3 position = getRandomCubePosition ();
		dest.transform.position = position;

		DestinationCubeBehaviour.createDestinationBehaviour(dest, listener);

	}

	private void completedTeaching() {
		GameObject.Destroy (movingCube);
		responder (successString);
	}

	private List<GestureID> setUpMove() {
		List<GestureID> list = new List<GestureID> ();
		
		list.Add (GestureID.MOVE);
		
		return list;
	}

	private Vector3 getRandomCubePosition() {
		float x = Random.Range (xBounds.x, xBounds.y);
		float y = Random.Range (yBounds.x, yBounds.y);
		float z = Random.Range (zBounds.x, zBounds.y);

		return new Vector3 (x, y, z);
	}
	
}






