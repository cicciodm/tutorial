﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;

class RotatingGestureTeacher : SingleGestureTeacher {
	private RespondToGestureTaught responder;
	private string successString;
	
	private GameObject rotatingCubePrefab;
	private GameObject rotatingCube;
	
	private GameObject targetQuadPrefab;
	
	private HandController controller;
	private TextWriter writer;
	
	private List<GestureID> toPerform;
	private TutorialModelModifier modifier;
	
	private GestureCompletionListener completionListener;
	
	private int rotStatus = 0;

	private List<string> toPrint;
	private List<CubeColours> colsToReach;

	private RotatingCubeBehaviour rotatingCubeBehaviour;

	
	public RotatingGestureTeacher (RespondToGestureTaught res, 
	                             string success,
	                             HandController controller,
	                             TextWriter writer) {
		this.responder = res;
		this.successString = success;

		this.rotatingCubePrefab = Resources.Load(TutorialResources.ROTATING_CUBE) as GameObject;
		this.targetQuadPrefab = Resources.Load(TutorialResources.TARGET_QUAD) as GameObject;

		this.toPrint = initToPrint ();
		this.colsToReach = initToReach ();
		this.toPerform = setUpRotate ();

		this.controller = controller;
		this.writer = writer;
	}
	
	public void teachGesture () {
		
		rotatingCube = GameObject.Instantiate (rotatingCubePrefab);
		rotatingCube.transform.position = Vector3.zero;
		
		modifier = TutorialModelModifier.createModelModifier (rotatingCube, controller, ModifierFlags.ROTATE);
		
		RespondToGestureCompletion res = moveCompleted;
		
		completionListener = new TutorialGestureCompletionListener (toPerform, res);
		GameObject targetQuad = GameObject.Instantiate (targetQuadPrefab );
		

		CubeColours col = colsToReach [rotStatus];

		rotatingCubeBehaviour = RotatingCubeBehaviour.createRotatingCube (rotatingCube,
		                                                                  col,
		                                                                  targetQuad,
		                                                                  completionListener);
		
		modifier.setCompletionListener (completionListener);
		
	}
	
	private void moveCompleted() {
		
		if (rotStatus == toPrint.Count) {
			completedTeaching ();
			
		} else {
			
			writer.writeText (toPrint[rotStatus]);

			rotStatus++;

			toPerform = setUpRotate ();
			completionListener.setGestureList (toPerform);
			CubeColours currColour = colsToReach[rotStatus];

			rotatingCubeBehaviour.setColourToReach(currColour);
		}
	}
	
	private void completedTeaching() {
		GameObject.Destroy (rotatingCube);
		responder (successString);
	}

	private List<string> initToPrint() {
		List<string> list = new List<string> ();

		list.Add (TutorialStrings.ROTATE_LEFT_COMPLETE);
		list.Add (TutorialStrings.ROTATE_RIGHT_COMPLETE);
		list.Add (TutorialStrings.ROTATE_UP_COMPLETE);

		return list;
	}

	private List<CubeColours> initToReach() {
		List<CubeColours> list = new List<CubeColours> ();
		
		list.Add (CubeColours.YELLOW);
		list.Add (CubeColours.WHITE);
		list.Add (CubeColours.GREEN);
		list.Add (CubeColours.BLUE);
		
		return list;
	}

	private List<GestureID> setUpRotate() {
		List<GestureID> list = new List<GestureID> ();
		
		list.Add (GestureID.ROTATE);
		
		return list;
	}
}






