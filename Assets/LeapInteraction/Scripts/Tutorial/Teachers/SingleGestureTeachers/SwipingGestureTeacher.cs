using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;

class SwipingGestureTeacher : SingleGestureTeacher
{
	private RespondToGestureTaught responder;
	private string successString;
	private GameObject multiSpherePrefab;
	private GameObject multiSphere;
	private HandController controller;
	private TextWriter writer;

	private List<GestureID> toPerform;
	private TutorialModelModifier modifier;

	public SwipingGestureTeacher (RespondToGestureTaught res, 
	                              string success,
	                              HandController controller,
	                              TextWriter writer) {
		this.responder = res;
		this.successString = success;
		this.multiSpherePrefab = Resources.Load(TutorialResources.MULTISPHERE) as GameObject;
	
		this.controller = controller;
		this.writer = writer;

		this.toPerform = setUpSwipes (GestureID.SWIPE_RIGHT);
	}

	public void teachGesture () {

		multiSphere = GameObject.Instantiate (multiSpherePrefab);

		modifier = TutorialModelModifier.createModelModifier (multiSphere, controller, ModifierFlags.SWIPE);

		RespondToGestureCompletion res = layersRemoved;
		GestureCompletionListener addListener = new TutorialGestureCompletionListener (toPerform, res);

		modifier.setCompletionListener (addListener);

	}

	private void layersRemoved() {
		writer.writeText (TutorialStrings.LAYERS_REMOVED);
		toPerform = setUpSwipes (GestureID.SWIPE_LEFT);

		RespondToGestureCompletion res = layersAdded;
		GestureCompletionListener removeListener = new TutorialGestureCompletionListener (toPerform, res);

		modifier.setCompletionListener (removeListener);
	}

	private void layersAdded() {
		GameObject.Destroy (multiSphere);
		responder (successString);
	}


	private List<GestureID> setUpSwipes(GestureID g) {
		List<GestureID> list = new List<GestureID> ();

		list.Add (g);
		list.Add (g);

		return list;
	}

}






