﻿using UnityEngine;
using System.Collections;

public class PointingGestureTeacher : SingleGestureTeacher {

	private RespondToGestureTaught responder;
	private string successString;

	private Vector3 buttonPos1 = new Vector3 (-5.1f, 0, 8.8f);
	private Vector3 buttonPos2 = new Vector3 (5.1f, 0, 8.8f);

	int buttonsTouched;

	public PointingGestureTeacher(RespondToGestureTaught responder, string success) {
		this.responder = responder;
		this.successString = success;
	}

	public void teachGesture() {

		buttonsTouched = 0;

		RespondToButtonPress res = buttonPressed;

		ButtonCreator.createButton (res, buttonPos1);
		ButtonCreator.createButton (res, buttonPos2);

	}

	private void buttonPressed() {
		buttonsTouched++;

		if (buttonsTouched == 2) {
			responder(successString);
		}
	}
}
