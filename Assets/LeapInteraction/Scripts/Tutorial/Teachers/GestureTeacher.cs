using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;

public class GestureTeacher {

	private Controller leap;
	private RespondToTeachingCompletion responder;
	private TextWriter writer;
	private HandController controller;

	private int status = 3;

	private KeyCode CONTINUE = KeyCode.Space;
	private KeyCode REPEAT = KeyCode.R;

	private List<KeyCode> keyCodes;

	public GestureTeacher(Controller leap,
	                      HandController controller,
	                      TextWriter writer, 
	                      RespondToTeachingCompletion responder){

		this.leap = leap;
		this.responder = responder;
		this.writer = writer;
		this.controller = controller;

		initKeyCodeList ();

	}

	public void teachGestures() {
		recursiveTeachGestures ();
	}

	private void recursiveTeachGestures() {

		RespondToGestureTaught res = gestureTaught;
		SingleGestureTeacher teacher;

		switch (status) {

		case (int)TeachingStatus.STATUS_POINT:
			teacher = new PointingGestureTeacher(res, TutorialStrings.G_POINT_TAUGHT);
			teachAGesture(teacher, TutorialStrings.G_POINT_TEACH);
			break;

		case (int)TeachingStatus.STATUS_SWIPE:
			teacher = new SwipingGestureTeacher(res, TutorialStrings.G_SWIPE_TAUGHT, controller, writer);
			teachAGesture(teacher, TutorialStrings.G_SWIPE_TEACH);
			break;
		
		case (int)TeachingStatus.STATUS_MOVE:
			teacher = new MovingGestureTeacher(res, TutorialStrings.G_MOVE_TAUGHT, controller, writer);
			teachAGesture(teacher, TutorialStrings.G_MOVE_TEACH);
			break;

		case (int)TeachingStatus.STATUS_ROTATE:
			teacher = new RotatingGestureTeacher(res, TutorialStrings.G_ROTATE_TAUGHT, controller, writer);
			teachAGesture(teacher, TutorialStrings.G_ROTATE_TEACH);
			break;

		}

	}

	private void teachAGesture(SingleGestureTeacher t, string before) {
		writer.writeText (before);

		t.teachGesture ();
	}

	private void gestureTaught(string success) {
		writer.writeText (success);
		RespondToInput res = inputReceived;

		GameObject reader = new GameObject ("InputReader");
		InputReader.createInputReader (reader, res, keyCodes);
	}

	public void inputReceived(KeyCode key) {
		if (key == CONTINUE) {
			status++;
		} else if (key != REPEAT) {
			throw new UnityException("There should not be other keys");
		}
		
		recursiveTeachGestures ();
	}

	private void initKeyCodeList() {
		keyCodes = new List<KeyCode> ();
		keyCodes.Add (CONTINUE);
		keyCodes.Add (REPEAT);
	}
}





