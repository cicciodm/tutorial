﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;

public class HandPoseTeacher {

	private Controller leap;
	private TextWriter writer;
	private RespondToTeachingCompletion responder;

	private List<Finger.FingerType> POINT_LIST;
	private List<Finger.FingerType> SWIPE_LIST;
	private List<Finger.FingerType> MOVE_LIST;
	private List<Finger.FingerType> ROTATE_LIST;
	
	private KeyCode REPEAT = KeyCode.R;
	private KeyCode CONTINUE = KeyCode.Space;
	
	private List<KeyCode> keyCodes;
	
	private int handPoseStatus = 0;

	public HandPoseTeacher(Controller leap, TextWriter writer, RespondToTeachingCompletion responder) {
		this.leap = leap;
		this.writer = writer;
		this.responder = responder;

		initFingerLists ();
		
		initKeyCodeList ();
	}

	public void teachHandPoses() {
		recursiveTeachHandPoses ();
	}

	private void recursiveTeachHandPoses() {
		
		switch (handPoseStatus) {
			
		case (int)TeachingStatus.STATUS_POINT:
			teachAHandPose (POINT_LIST, TutorialStrings.POINT_TEACH, TutorialStrings.POINT_TAUGHT);
			break;
			
		case (int)TeachingStatus.STATUS_SWIPE:
			teachAHandPose (SWIPE_LIST, TutorialStrings.SWIPE_TEACH, TutorialStrings.SWIPE_TAUGHT);
			break;
			
		case (int)TeachingStatus.STATUS_MOVE:
			teachAHandPose (MOVE_LIST, TutorialStrings.MOVE_TEACH, TutorialStrings.MOVE_TAUGHT);
			break;
			
		case (int)TeachingStatus.STATUS_ROTATE:
			teachAHandPose (ROTATE_LIST, TutorialStrings.ROTATE_TEACH, TutorialStrings.ROTATE_TAUGHT);
			break;
			
		default:
			responder();
			break;
		}
		
	}
	
	private void teachAHandPose(List<Finger.FingerType> fingerList, string before, string after) {
		
		writer.writeText (before);
		
		GameObject poseRec = new GameObject ("HandPoseRecogniser");

		RespondToPoseRecognition res = handPoseTaught;

		//Pose recognition will be done here
		HandPoseRecogniser.createPoseRecogniser (poseRec, res, leap, fingerList, after);
		
	}
	
	public void handPoseTaught(string success) {
		
		writer.writeText (success);
		
		RespondToInput res = inputReceived; 
		
		GameObject reader = new GameObject ("InputReader");
		
		InputReader.createInputReader (reader, res, keyCodes);
	}
	
	public void inputReceived(KeyCode key) {
		
		if (key == CONTINUE) {
			handPoseStatus++;
		} else if (key != REPEAT) {
			throw new UnityException("There should not be other keys");
		}
		
		recursiveTeachHandPoses ();
	}
	
	private void initFingerLists() {
		POINT_LIST = new List<Finger.FingerType> ();
		POINT_LIST.Add (Finger.FingerType.TYPE_INDEX);
		
		SWIPE_LIST = new List<Finger.FingerType> ();
		SWIPE_LIST.Add (Finger.FingerType.TYPE_INDEX);
		SWIPE_LIST.Add (Finger.FingerType.TYPE_MIDDLE);
		
		MOVE_LIST = new List<Finger.FingerType> ();
		MOVE_LIST.Add (Finger.FingerType.TYPE_INDEX);
		MOVE_LIST.Add (Finger.FingerType.TYPE_MIDDLE);
		MOVE_LIST.Add (Finger.FingerType.TYPE_THUMB);
		
		ROTATE_LIST = new List<Finger.FingerType> ();
		ROTATE_LIST.Add (Finger.FingerType.TYPE_THUMB);
		ROTATE_LIST.Add (Finger.FingerType.TYPE_INDEX);
		ROTATE_LIST.Add (Finger.FingerType.TYPE_MIDDLE);
		ROTATE_LIST.Add (Finger.FingerType.TYPE_RING);
		ROTATE_LIST.Add (Finger.FingerType.TYPE_PINKY);
	}
	
	private void initKeyCodeList() {
		keyCodes = new List<KeyCode> ();
		keyCodes.Add (CONTINUE);
		keyCodes.Add (REPEAT);
	}

}
