﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class NullGestureCompletionListener : GestureCompletionListener {

	public void gesturePerformed(GestureID gesture) {
		//Do nothing
	}

	public void allGesturesCompleted() {
		//Do nothing
	}

	public void setGestureList(List<GestureID> list) {
		//Do nothing
	}
}
