﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface GestureCompletionListener {

	void gesturePerformed(GestureID gesture);

	void allGesturesCompleted();

	void setGestureList(List<GestureID> list);
}
