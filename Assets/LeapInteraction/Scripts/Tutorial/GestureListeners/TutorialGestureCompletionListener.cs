﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TutorialGestureCompletionListener : GestureCompletionListener {

	private List<GestureID> gestures;
	private RespondToGestureCompletion responder;

	public TutorialGestureCompletionListener(List<GestureID> gestures,
	                                         RespondToGestureCompletion responder) {
		this.gestures = gestures;
		this.responder = responder;
	}

	public void gesturePerformed(GestureID id) {

		if(id == gestures[0]) {
			gestures.RemoveAt(0);
		}

		if (gestures.Count == 0) {
			allGesturesCompleted();
		}
	}

	public void allGesturesCompleted() {
		responder ();
	}

	public void setGestureList(List<GestureID> list) {
		this.gestures = list;
	}
}
