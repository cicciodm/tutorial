﻿using UnityEngine;
using System.Collections;
using Leap;

public class TutorialController : MonoBehaviour {

	private TutorialPerformer tut;
	private Controller leap;
	public HandController controller;

	// Use this for initialization
	void Start () {
		leap = new Controller ();
		leap.EnableGesture (Gesture.GestureType.TYPE_SWIPE);
		tut = new TutorialPerformer (leap, controller);
		tut.startTutorial ();
	}
}
