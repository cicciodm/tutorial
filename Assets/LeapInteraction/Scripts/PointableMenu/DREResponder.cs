﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;
using DG.Tweening;

public class DREResponder : MenuMonoBehaviour {

	private Vector3 INITIAL_COLLIDER_SIZE;
	private Vector3 LARGE_COLLIDER_SIZE = new Vector3(10,10,10);
	private MenuManager menuManager;

	public static DREResponder createResponder(GameObject target, 
	                                           string text,  
	                                           Vector3 lookTowards,
	                                           MenuManager manager){
		DREResponder resp = target.AddComponent<DREResponder> ();

		resp.menuManager = manager;
		resp.setMenuText (text);
		target.transform.LookAt (lookTowards);
		resp.INITIAL_COLLIDER_SIZE = target.GetComponent<BoxCollider> ().size;
		
		return resp;
	}
	
	void OnTriggerEnter(Collider other) {
		if (other.tag == Tags.MENU_POINTER) {
			setColliderSize(LARGE_COLLIDER_SIZE);
			gameObject.transform.DOPunchScale(new Vector3(0.5f, 0.5f, 0f), 0.5f, 1, 0f)
				.OnComplete(() => 
					{
						menuManager.toggleDRE();
						setColliderSize(INITIAL_COLLIDER_SIZE);
					});
		}
	}

	private void setColliderSize(Vector3 newSize) {
		BoxCollider collider = gameObject.GetComponent<BoxCollider>();
		collider.size = newSize;
	}
}
