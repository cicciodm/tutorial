﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Leap;

public class PointableMenu : MonoBehaviour {
	Controller leap;

	private HandController controller;
	private Vector3 menuCoords;

	private GameObject pointer;

	private int FRAME_WINDOW = 5;
	private Vector3MovingAverager avg;

	private FloatingText ft;

	private List<string> fileNames;

	private const float SWIPE_SPEED = 200f;
	
	public static PointableMenu createPointableMenu(GameObject target, 
	                                                List<string> fileNames, 
	                                                Vector3 menuCoords,
	                                                HandController controller) {
		PointableMenu menu = target.AddComponent<PointableMenu> ();
		menu.fileNames = fileNames;
		menu.menuCoords = menuCoords;
		menu.controller = controller;
		menu.setUpMenuFeatures ();

		return menu;
	}

	// Use this for initialization
	void Start () {

		leap = new Controller ();
		configureLeap();

		avg = new Vector3MovingAverager (FRAME_WINDOW);

		Vector3 cameraPos = GameObject.FindGameObjectWithTag ("MainCamera").transform.position;

		ft = new FloatingText.FloatingTextBuilder ()
							 .lookingTowards(cameraPos)
							 .buildFloatingText ();
	}

	public void setUpMenuFeatures() {
		Vector3 cameraPos = GameObject.FindGameObjectWithTag ("MainCamera").transform.position;

		MenuManager menuManager = new MenuManager (controller, fileNames, cameraPos);
		
		menuManager.createMainMenu("Models", menuCoords);
		
		menuManager.createDREButton ("DRE");

//		menuManager.createTransparencySlider("Transparency");

		menuManager.loadModel (fileNames [0]);
		
		pointer = Instantiate (Resources.Load (ResourceNames.MENU_POINTER) as GameObject) as GameObject;

		ClearMenuOnDestroy.createDestroyer (pointer, menuManager);

		pointer.SetActive (false);
	}

	// Update is called once per frame
	void Update () {
		//Track only one hand at the time
		EnhancedHand hand = HandManager.getLeftEnhancedHand (leap);
		if (hand.isPointing()) {
			Finger frontMost = hand.getFrontMostFinger();
			trackFinger(frontMost, hand.getLeapHand().IsLeft);

		} else {
			if(pointer.activeSelf) {
				pointer.SetActive(false);
				avg.resetMeasurements();
				ft.resetFloatingText();
			}
		}
	}

	public void trackFinger(Finger frontMost, bool isLeftHand) {
		Vector3 tipPosition = Converter.LeapToWorldPoint(controller, frontMost.TipPosition);
		avg.updateAverage (tipPosition);

		if (avg.isStableAverage ()) {
			pointer.SetActive(true);
			Vector3 pointerPos = avg.getAverage();
			pointer.transform.position = pointerPos;
			pointer.tag = Tags.MENU_POINTER;

			ft.updateFloatingText(pointerPos, frontMost.Id, 
			                      Strings.POINTING_STATE, Strings.POINTING_DESC, isLeftHand);
		}
	}

	private void configureLeap () {
		leap.EnableGesture (Gesture.GestureType.TYPE_SWIPE);
		leap.Config.SetFloat ("Gesture.Swipe.MinVelocity", SWIPE_SPEED);
		leap.Config.Save ();
	}
}
