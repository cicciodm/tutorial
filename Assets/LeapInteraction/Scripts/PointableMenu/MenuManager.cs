﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class MenuManager {

	private HandController controller;
	private GameObject menuObject;

	private List<string> fileNames;

	private Tuple<Vector3, Vector3>[] subMenuElemsPositions = null;
	private float MENU_RADIUS_BEFORE;
	private float MENU_RADIUS;
	private float TOTAL_DEGREES = 280;

	private Vector3 lookTowards;

	private Quaternion rotationAngle;

	private ModelModifier currentModelModifier;
	private bool isDRELoaded = false;
	

	private SettableTransform patientStandingTransform;
	private SettableTransform patientDRETransform;
	private SettableTransform bodyPartsDRETransform;
	
	private GameObject patient;
	private GameObject femurs;

	
	//Constructor, sets the transform parameters
	public MenuManager(HandController controller, List<string> fileNames, Vector3 lookTowards) {
		this.controller = controller;
		this.fileNames = fileNames;
		this.lookTowards = lookTowards;

		patientStandingTransform = new SettableTransform ();
		patientStandingTransform.setPosition(new Vector3 (0, -5, -20));
		patientStandingTransform.setEulerAngles(new Vector3 (0, 0, 0));

		patientDRETransform = new SettableTransform ();
		patientDRETransform.setPosition(new Vector3 (5.65f, -1.91f, -34.84f));
		patientDRETransform.setEulerAngles(new Vector3 (86.26f, -5.24f, 13.28f));
	}

	//Creation methods for Main Menu, subMenus, DRE button
	public void createMainMenu(string name, Vector3 menuCoords) {
		GameObject obj = Resources.Load(ResourceNames.MENU_TEXT_MAIN) as GameObject;
		menuObject = GameObject.Instantiate (obj, menuCoords, Quaternion.identity) as GameObject;
		MainMenuResponder.createResponder (menuObject, name, lookTowards, this);

		rotationAngle = Quaternion.FromToRotation (Vector3.back, menuObject.transform.forward);

		float menuXScale = (float)menuObject.transform.GetChild (0).transform.localScale.x;
		this.MENU_RADIUS_BEFORE =  menuXScale;
		this.MENU_RADIUS = 1.5f * menuXScale;
	}

	public void createSubMenus() {

		if (subMenuElemsPositions == null) {
			subMenuElemsPositions = getPositionsForElements (menuObject.transform.position, 
			                                                 fileNames.Count);
		}
		
		for(int i = 0; i < subMenuElemsPositions.Length; i++) {
			GameObject obj = Resources.Load(ResourceNames.MENU_TEXT_SUB) as GameObject;
			GameObject copy = GameObject.Instantiate(obj, subMenuElemsPositions[i].First, 
			                                         Quaternion.identity) as GameObject;
			
			SubMenuResponder.createResponder(copy, fileNames[i], subMenuElemsPositions[i].First, lookTowards, this);
			copy.tag = Tags.MENU_ELEM;
			copy.transform.DOMove(subMenuElemsPositions[i].Second, 0.3f);
			
		}
	}

	public void createDREButton(string name) {
		Vector3 menuPos = menuObject.transform.position;
		Vector3 drePos = new Vector3 (menuPos.x - 2, menuPos.y + 0.5f, menuPos.z);

		Vector3 drePosRotated = rotateAroundPoint (drePos, menuPos, rotationAngle);

		GameObject obj = Resources.Load (ResourceNames.MENU_TEXT_MAIN) as GameObject;
		GameObject dre = GameObject.Instantiate (obj, drePosRotated, Quaternion.identity) as GameObject;

		DREResponder.createResponder (dre, name, lookTowards, this);

		toggleSliderView (Tags.DRE_SLIDER);
	}


	public void resetMenu() {
		clearSubMenus ();
		MainMenuResponder resp = menuObject.GetComponent<MainMenuResponder> ();
		resp.restore ();
	}

	public void clearSubMenus() {
		clearSubMenus (() => {});
	}

	public void clearSubMenus(Action callback) {
		GameObject[] subButtons = GameObject.FindGameObjectsWithTag (Tags.MENU_ELEM); 
		foreach (GameObject o in subButtons) {
			SubMenuResponder resp = o.GetComponent<SubMenuResponder>();
			resp.moveBackAndDestroy(callback);
		}
	}

	public void deactivateSubMenus() {
		foreach (GameObject o in GameObject.FindGameObjectsWithTag (Tags.MENU_ELEM)) {
			o.GetComponent<Collider>().enabled = false;
		}
	}

	public void loadModel(string fileName) {
		unLoadModel ();
		patient = new GameObject ("Patient");

		string skinToLoad = isDRELoaded ? ResourceNames.SKIN_DRE : ResourceNames.SKIN_STANDING;
		SettableTransform patientTransform = isDRELoaded ? patientDRETransform : patientStandingTransform;

		GameObject skin = GameObject.Instantiate(Resources.Load(skinToLoad)) as GameObject;
		skin.transform.parent = patient.transform;

		GameObject bodyParts = new GameObject("BodyParts");
		bodyParts.tag = Tags.BODY_PARTS;
		bodyParts.transform.parent = patient.transform;

		GameObject anatomy = GameObject.Instantiate(Resources.Load(ResourceNames.ANATOMY)) as GameObject;
		anatomy.transform.parent = bodyParts.transform;
		femurs = getFemurs (anatomy);

		if (isDRELoaded) {
			femurs.SetActive(false);
		}

		GameObject organs = Resources.Load (ResourceNames.MODELS_FOLDER + fileName) as GameObject;
		GameObject copy = GameObject.Instantiate (organs) as GameObject;
		copy.transform.parent = bodyParts.transform;

		currentModelModifier = ModelModifier.createModelModifier (patient, controller, this);

		patientTransform.attachToGameObject(patient);
		patient.tag = Tags.LOADED_MODEL;
		
		resetMenu ();
	}

	private GameObject getFemurs(GameObject anatomy) {
		GameObject skeleton = anatomy.transform.GetChild (1).gameObject;
		return skeleton.transform.GetChild (3).gameObject;
	}

	public void toggleDRE() {
		//Select the rotation
		isDRELoaded = !isDRELoaded;

		SettableTransform patientTransform = isDRELoaded ? patientDRETransform : patientStandingTransform;
	
		string skinToLoad = isDRELoaded ? ResourceNames.SKIN_DRE : ResourceNames.SKIN_STANDING;
		string skinToDestroyTag = isDRELoaded ? Tags.SKIN_STANDING : Tags.SKIN_DRE;
		 

		GameObject.DestroyImmediate (GameObject.FindGameObjectWithTag (skinToDestroyTag));
		GameObject skinPrefab = Resources.Load (skinToLoad) as GameObject;
		GameObject newSkin = GameObject.Instantiate (skinPrefab, patient.transform.position, patient.transform.rotation) as GameObject;

		newSkin.transform.parent = patient.transform;

		//We need the skin as first element
		newSkin.transform.SetSiblingIndex (0);
		patientTransform.attachToGameObject(patient);
		setTableStatus (isDRELoaded);

		femurs.SetActive (!femurs.activeSelf);

		toggleSliderView (Tags.SLIDER);
		toggleSliderView (Tags.DRE_SLIDER);

		currentModelModifier.toggleDRE (patient);
	}

	private void toggleSliderView(string tag) {
		GameObject slider = GameObject.FindGameObjectWithTag (tag);
		toggleRenderers (slider);
	}

	private void toggleRenderers(GameObject obj) {
		Renderer[] rs = obj.GetComponentsInChildren<Renderer> ();
		foreach (Renderer r in rs) {
			r.enabled = !r.enabled;
		}
	}

	public bool getDREStatus() {
		return isDRELoaded;
	}

	private void setTableStatus(bool status) {
		GameObject table = GameObject.FindGameObjectWithTag (Tags.TABLE);
		Renderer[] rs = table.GetComponentsInChildren<Renderer> (status);
		foreach (Renderer r in rs) {
			r.enabled = status;
		}
	}

	private void unLoadModel() {
		GameObject.Destroy(GameObject.FindWithTag(Tags.LOADED_MODEL));
	}
	
	private Tuple<Vector3, Vector3>[] getPositionsForElements(Vector3 centre, int elems) {
		Tuple<Vector3, Vector3>[] positions = new Tuple<Vector3, Vector3>[elems];
		
		// Stuff will be placed in a semi-circle;
		// Each element will have its own space
		float degForEach = TOTAL_DEGREES / elems;
		float currentAngle = -degForEach/2;
		
		for (int i = 0; i < elems; i++) {
			Vector3 before = getVectorInCircle(centre, MENU_RADIUS_BEFORE, currentAngle);
			Vector3 after = getVectorInCircle(centre, MENU_RADIUS, currentAngle);
			positions[i] = Tuple.New(before, after);
			currentAngle = (currentAngle + degForEach) % 360;
		}
		
		return positions;
	}
	
	private Vector3 getVectorInCircle(Vector3 centre, float radius, float ang){ 
		Vector3 pos = new Vector3(); 
		pos.x = centre.x + radius * Mathf.Sin(ang * Mathf.Deg2Rad); 
		pos.y = centre.y + radius * Mathf.Cos(ang * Mathf.Deg2Rad); 
		pos.z = centre.z;

		Vector3 rotated = rotateAroundPoint (pos, centre, rotationAngle);

		return rotated; 
	}

	private Vector3 rotateAroundPoint(Vector3 point, Vector3 pivot, Quaternion angle) {
		return angle * ( point - pivot) + pivot;
	}
}
