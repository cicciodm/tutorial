﻿using UnityEngine;
using System.Collections;

public class ClearMenuOnDestroy : MonoBehaviour {

	private MenuManager manager;

	public static ClearMenuOnDestroy createDestroyer(GameObject target, MenuManager manager) {
		ClearMenuOnDestroy clear = target.AddComponent<ClearMenuOnDestroy>();
		clear.manager = manager;
		return clear;
	}

	void OnDisable() {
		manager.resetMenu ();
	}
}
