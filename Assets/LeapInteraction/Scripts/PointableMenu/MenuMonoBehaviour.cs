﻿using UnityEngine;
using System.Collections;

public class MenuMonoBehaviour : MonoBehaviour {

	public void setMenuText(string text) {
		GameObject textObject = transform.GetChild(1).gameObject;
		textObject.GetComponent<TextMesh> ().text = text;
	}
}
