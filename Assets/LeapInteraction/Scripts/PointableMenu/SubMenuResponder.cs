﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;
using DG.Tweening;

public class SubMenuResponder : MenuMonoBehaviour {

	private string fileName;
	private MainMenuResponder parent;
	private Vector3 originalPos;
	private MenuManager menuManager;

	public static SubMenuResponder createResponder(GameObject target, 
	                                               string fileName,
	                                               Vector3 pos,
	                                               Vector3 lookTowards,
	                                               MenuManager manager){
		SubMenuResponder resp = target.AddComponent<SubMenuResponder> ();
		resp.menuManager = manager;
		resp.fileName = fileName;
		resp.originalPos = pos;
		resp.setMenuText (resp.getTextForMenu (fileName));
		
		target.transform.LookAt (lookTowards);

		return resp;
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == Tags.MENU_POINTER) {
			menuManager.deactivateSubMenus();
			gameObject.transform.DOPunchScale(new Vector3(0.5f, 0.5f, 0f), 0.5f, 1, 0f)
				.OnComplete(() => menuManager.loadModel(fileName));
		}
	}

	public void moveBackAndDestroy(Action callback) {
		moveBackAndDestroyWithDelay (0.2f, callback);
	}

	public void moveBackAndDestroyWithDelay(float delay, Action callback) {
		gameObject.transform.DOMove (originalPos, 0.3f)
							.SetDelay (delay)
							.OnComplete (() => { destroy (); callback ();});
	}

	private void destroy() {
		Destroy (gameObject);
	}

	private string getTextForMenu(string file) {

		string[] words = file.Split (' ');

		if (words.Length == 1 && words [0].Length <= 5) {
			return words[0];
		}

		int toDisplay = (words.Length <= 3) ? words.Length : 3;
		StringBuilder sb = new StringBuilder ();

		for (int i = 0; i < toDisplay; i++) {
			string curr = words[i];
			sb.Append(curr[0]);
		}

		return sb.ToString ();

	}
}
