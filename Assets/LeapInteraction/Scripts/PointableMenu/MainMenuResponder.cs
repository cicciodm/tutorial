﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class MainMenuResponder : MenuMonoBehaviour {

	private Vector3 INITIAL_COLLIDER_SIZE;
	private Vector3 LARGE_COLLIDER_SIZE = new Vector3(10,10,10);
	private float APLHA_OPAQUE = 1f;
	private float ALPHA_TRANSPARENT = 0.3f;
	private MenuManager menuManager;

	public static MainMenuResponder createResponder(GameObject target, 
	                                                string text, 
	                                                Vector3 lookTowards,
	                                                MenuManager manager) {

		MainMenuResponder resp = target.AddComponent<MainMenuResponder> ();
		resp.menuManager = manager;
		resp.setMenuText (text);
		resp.INITIAL_COLLIDER_SIZE = target.GetComponent<BoxCollider> ().size;
		target.transform.LookAt (lookTowards);

		return resp;
	}

	void OnTriggerEnter(Collider other) {
		if (other.tag == Tags.MENU_POINTER) {
			setOpacity(ALPHA_TRANSPARENT);
			setColliderSize(LARGE_COLLIDER_SIZE);
			menuManager.createSubMenus();
		}
	}

	void OnTriggerExit(Collider other) {
		if (other.tag == Tags.MENU_POINTER) {
			menuManager.clearSubMenus (restore);
		}
	}

	public void restore() {
		setColliderSize (INITIAL_COLLIDER_SIZE);
	}

	public void setOpacity(float alpha) {
//		Renderer r = gameObject.GetComponent<Renderer> ();
//		r.material.DOFade (alpha, 0.3f);
	}

	private void setVisibility(bool visible) {
		Renderer r = gameObject.GetComponent<Renderer> ();
		r.enabled = visible;
	}

	private void setColliderSize(Vector3 newSize) {
		BoxCollider collider = gameObject.GetComponent<BoxCollider>();
		collider.size = newSize;
	}


}
