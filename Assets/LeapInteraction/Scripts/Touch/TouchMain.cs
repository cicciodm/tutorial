﻿using UnityEngine;
using System.Collections;
using Leap;

public class TouchMain : MonoBehaviour {

	//Thiss will need to be passed by the main script
	private HandController controller;

	private Controller leap;
	private GameObject probingTip;


	public static TouchMain createTouchMain(GameObject target, HandController controller) {
		TouchMain touch = target.AddComponent<TouchMain> ();
		touch.controller = controller;
		return touch;
	}

	// Use this for initialization
	void Start () {
		leap = new Controller ();

		probingTip = GameObject.Instantiate (Resources.Load (ResourceNames.PROBE)) as GameObject;
		probingTip.tag = Tags.PROBE;
		probingTip.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		EnhancedHand hand = HandManager.getRightEnhancedHand (leap);

		if (hand.isPointing ()) {
			probingTip.SetActive (true);
			Vector3 pos = hand.getV3FrontTipPosition (controller);
			probingTip.transform.position = pos;

		} else {
			probingTip.SetActive(false);
		}
	}
}
