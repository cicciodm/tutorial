﻿using UnityEngine;
using System.Collections;

public class PrintOnTouch : MonoBehaviour {

	private Color original;
	private Color highlight;

	private bool selected;
	private Collider otherCollider;

	private FloatingText ft;

	// Use this for initialization
	void Start () {
		highlight = colorFromVals (77, 249, 92);

		Renderer renderer = GetComponent<Renderer> ();
		if (renderer != null) {
			original = renderer.material.color;
		} else {
			original = Color.white;
		}
	}

	void Update() {
		if (selected) {
			Vector3 fingerPosition = otherCollider.gameObject.transform.position;
			TouchController.getInstance().updateTextPosition(fingerPosition);
		} 
	}

	void OnTriggerEnter(Collider other){
		if (other.tag == Tags.PROBE) {
			selected = true;
			otherCollider = other;
			TouchController.getInstance().newTextTriggered(name, this);
			setColour(highlight);
		}
	}

	void OnTriggerExit(Collider other){
		if (other.tag == Tags.PROBE) {
			selected = false;
			setColour(original);
			TouchController.getInstance().finishedTouch(name, this);
		}
	}

	public void resetColour() {
		setColour (original);
	}

	private void setColour(Color col) {
		Renderer r = GetComponent<Renderer>();
		if (r != null) {
			r.material.color = col;
		}
	}

	private Color colorFromVals(float r, float g, float b){
		return new Color (r / 256, g / 256, b / 265);
	}

}
