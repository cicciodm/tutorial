﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TouchController {


	private static TouchController instance = null;
	private FloatingText ft;
	private int selectCount;

	private string currFirst = null;
	private string currSecond = null;

	private HashSet<PrintOnTouch> touching;

	public static TouchController getInstance() {
		if (instance == null) {
			instance = new TouchController();
		}
		return instance;
	}

	private TouchController() {
		Vector3 cameraPos = GameObject.FindGameObjectWithTag ("MainCamera").transform.position;
		ft = new FloatingText.FloatingTextBuilder ()
			.withTextPrefabName(ResourceNames.BLACK_TEXT_ST)
			.lookingTowards(cameraPos)
			.buildFloatingText ();
		touching = new HashSet<PrintOnTouch> ();
	}

	public void newTextTriggered(string text, PrintOnTouch touch) {
		if(currFirst != null) {
			currSecond = currFirst;
		}
		currFirst = text;

		touching.Add (touch);
	}

	public void updateTextPosition(Vector3 tipPosition) {
		ft.updateFloatingText (tipPosition,
		                      selectCount,
		                      currFirst,
		                      currSecond,
		                      false);
	}

	public void finishedTouch(string text, PrintOnTouch p) {
		if (text.Equals (currFirst)) {
			currFirst = currSecond;
			currSecond = null;
		} else if (text.Equals (currSecond)) {
			currSecond = null;
		}

		if (currFirst == null && currSecond == null) {
			ft.resetFloatingText();
		}
		touching.Remove (p);
		selectCount++;
	}

	public void probeDisabled() {
		ft.resetFloatingText ();

		foreach (PrintOnTouch p in touching) {
			p.resetColour();
		}

		touching.Clear ();
	}
}
