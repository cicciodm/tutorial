using UnityEngine;
using System.Collections;
using System;

public class Vector3MovingAverager {
	private int window;
	private Queue valuesQueue;
	private Vector3 movingAverage;
	
	public Vector3MovingAverager(int windowSize) {
		window = windowSize;
		valuesQueue = new Queue ();
	}

	public void updateAverage(Vector3 value) {
		if (valuesQueue.Count == window) {
			Vector3 old = (Vector3)valuesQueue.Dequeue ();
			movingAverage -= (old / window);		
		}

		valuesQueue.Enqueue (value);
		movingAverage += (value / window);
	}

	public bool isStableAverage() {
		return valuesQueue.Count == window;
	}

	public Vector3 getAverage() {
		return movingAverage;
	}

	public void resetMeasurements() {
		valuesQueue.Clear();
		movingAverage = Vector3.zero;
	}

}

