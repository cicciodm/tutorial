﻿using UnityEngine;
using System.Collections;
using System;

public class FloatMovingAverager {
	private int window;
	private Queue valuesQueue;
	private float movingAverage;
	
	public FloatMovingAverager(int windowSize) {
		window = windowSize;
		valuesQueue = new Queue ();
	}
	
	public void updateAverage(float value) {
		if (valuesQueue.Count == window) {
			float old = (float)valuesQueue.Dequeue ();
			movingAverage -= (old / window);		
		}
		
		valuesQueue.Enqueue (value);
		movingAverage += (value / window);
	}

	public bool isStableAverage() {
		return valuesQueue.Count == window;
	}

	public float getAverage() {
		return movingAverage;
	}

	public void resetMeasurements() {
		valuesQueue.Clear();
		movingAverage = 0f;
	}

}

