﻿using UnityEngine;
using System.Collections;
using Leap;

public class LineDrawer : MonoBehaviour {
	public HandController controller;

	void Start () {

	}	

	public static void drawPalmNormal(Hand hand, HandController controller) {
		Vector3 palmNormal = 5 * Converter.LeapToWorldDirection(controller, hand.PalmNormal);
		Debug.DrawRay(Vector3.zero, palmNormal, Color.red);
	}

	public static void drawPalmDirection(Hand hand, HandController controller) {
		Vector3 palmNormal = 5 * Converter.LeapToWorldDirection(controller, hand.Direction);
		Debug.DrawRay(Vector3.zero, palmNormal, Color.red);
	}

	public static void drawPalmVelocity(Hand hand, HandController controller) {
		Vector3 palmVelocity = Converter.LeapToWorldDirection(controller, hand.PalmVelocity) / 10;
		Debug.DrawRay(Vector3.zero, palmVelocity, Color.red);
	}



	// Update is called once per frame
//	void Update () {
//		Frame frame = leap.Frame ();
//		HandList hands = frame.Hands;
//
//		foreach (Hand hand in hands) {
//			Vector3 palmPos = Converter.LeapToWorldPoint (controller, hand.PalmPosition);
//			Debug.DrawRay (Vector3.zero, palmPos, Color.blue);
//
//			int extended = hand.Fingers.Extended().Count;
//
//			if (extended == 0) {
//				continue;
//			}
//
//			if (extended <= 2) {
//				Finger index = hand.Fingers.Frontmost;
//				Vector3 tip = Converter.LeapToWorldPoint(controller, index.TipPosition);
//				Vector3 dir = Converter.LeapToWorldDirection(controller, index.Direction);
//				Debug.DrawRay(tip, 5 * dir, Color.yellow);
//
//			} else {
//				Vector3 palmNormal = 8 * Converter.LeapToWorldDirection(controller, hand.PalmNormal);
//				Debug.DrawRay(palmPos, palmNormal, Color.red);
//			}
//		}
//	}



}
