﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Leap;

public class EnhancedHand {
	
	private Hand theHand;
	private float PINCH_THRESH = 0.85f;
	
	public EnhancedHand(Hand hand) {
		theHand = hand;
	}
	
	public Hand getLeapHand() {
		return theHand;
	}
	
	public int getExtendedFingerCount() {
		return theHand.Fingers.Extended ().Count;
	}
	
	public FingerList getExtendedFingers() {
		return theHand.Fingers.Extended ();
	}
	
	public List<Finger.FingerType> getExtendedFingerTypes() {
		List<Finger.FingerType> types = new List<Finger.FingerType> ();
		
		FingerList extended = getExtendedFingers ();
		
		foreach (Finger f in extended) {
			types.Add(f.Type);
		}
		return types;
	}
	
	public bool isOpen() {
		return getExtendedFingerCount() == 5;
	}
	
	public Vector3 getPalmNormal(HandController controller) {
		return Converter.LeapToWorldDirection(controller, theHand.PalmNormal);
	}
	
	public Vector3 getPalmDirection(HandController controller) {
		return Converter.LeapToWorldDirection(controller, theHand.Direction);
	}
	
	public Vector3 getPalmPosition(HandController controller) {
		return Converter.LeapToWorldPoint(controller, theHand.PalmPosition);
	}
	
	public Vector3 getPalmVelocity(HandController controller) {
		return Converter.LeapToWorldDirection (controller, theHand.PalmVelocity);
	}
	
	public Finger getFrontMostFinger ()    {
		return theHand.Fingers.Frontmost;
	}
	
	public Vector getFrontTipPosition () {
		return theHand.Fingers.Frontmost.TipPosition;
	}
	
	public Vector3 getV3FrontTipPosition (HandController controller) {
		return Converter.LeapToWorldPoint (controller, getFrontTipPosition ());
	}
	
	public bool isPinching() {
		return theHand.PinchStrength >= PINCH_THRESH;
	}
	
	public bool isPointing() {
		Finger.FingerType fingerType = theHand.Fingers.Frontmost.Type;
		return getExtendedFingerCount () == 1 &&
			fingerType == Finger.FingerType.TYPE_INDEX;
	}
}