﻿using UnityEngine;
using System.Collections;
using Leap;

public class HandManager {

	public static Hand getLeftHand(Controller leap) {
		Frame frame = leap.Frame ();
		HandList hands = frame.Hands;
		return getLeftHandFromList (hands);
	}
	
	public static EnhancedHand getLeftEnhancedHand(Controller leap) {
		return new EnhancedHand(getLeftHand(leap));
	}

	public static Hand getRightHand(Controller leap) {
		Frame frame = leap.Frame ();
		HandList hands = frame.Hands;
		return getRightHandFromList (hands);
	}
	
	public static EnhancedHand getRightEnhancedHand(Controller leap) {
		return new EnhancedHand(getRightHand(leap));
	}

	private static Hand getLeftHandFromList(HandList hands) {
		return getHand (hands, true);
	}
	
	private static Hand getRightHandFromList(HandList hands) {
		return getHand (hands, false);
	}

	private static Hand getHand(HandList hands, bool wantLeft) {
		foreach (Hand hand in hands) {
			bool found = wantLeft ? hand.IsLeft : hand.IsRight;
			if(found) {
				return hand;
			} 
		}
		return Hand.Invalid;
	}
}
