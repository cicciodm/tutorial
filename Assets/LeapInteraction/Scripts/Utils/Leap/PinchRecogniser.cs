﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Leap;

public class PinchRecogniser {
	
	private int numFingersStart;
	private int numFingersEnd;
	private Queue<int> extendedFingerQueue;
	private Queue<int> pinchFingerQueue;
	private int frameWindow;
	private int validStartFrames;
	private int validEndFrames;
	private HandController controller;

	private EnhancedHand lastHand;
	private List<Finger.FingerType> lastPinchingFingers;

	private const float DISTANCE_CLOSE = 3f;
	private int CONFIDENCE_FRAMES = 25;

	//Each time there is an update, calculate both the extended fingers and the

	public PinchRecogniser(int start, int end, int window, HandController ctrl) {

		if(!(start >= 2 && end >= 2)){
			throw new UnityException("Pinching must start and end with at least 2 fingers");
		}

		extendedFingerQueue = new Queue<int> ();
		pinchFingerQueue = new Queue<int> ();
		numFingersStart = start;
		numFingersEnd = end;
		frameWindow = window;
		controller = ctrl;
		lastHand = new EnhancedHand(new Hand ());
		lastPinchingFingers = new List<Finger.FingerType> ();
	}

	public void updateWithHand(EnhancedHand hand) {
		lastHand = hand;
		updateValidStartFrames ();
	
		lastPinchingFingers = calculatePinchingFingers (); //Based on the last hand
		updateValidEndFrames ();
	}
		
	public bool isPinchingRecognised() {
		return validStartFrames > 0 && validEndFrames >= 13;
	}

	public List<Finger.FingerType> getPinchingFingers() {
		return lastPinchingFingers;
	}

	private List<Finger.FingerType> calculatePinchingFingers() {
		List<Finger.FingerType> pinchingFingerList = new List<Finger.FingerType> ();

		if (lastHand.isPinching ()) {

			//The thumb is always part of a pinch
			pinchingFingerList.Add(Finger.FingerType.TYPE_THUMB);

			FingerList fingers = lastHand.getLeapHand().Fingers;

			//Thumb is needed to calculate distance
			FingerList thumbList = lastHand.getLeapHand().Fingers.FingerType(Finger.FingerType.TYPE_THUMB);
			Vector3 thumbPosition = Converter.LeapToWorldPoint(controller, thumbList[0].TipPosition);

			foreach (Finger finger in fingers) {
				if (finger.Type != Finger.FingerType.TYPE_THUMB) {
					Vector3 position = Converter.LeapToWorldPoint(controller, finger.TipPosition);
					float dist = Vector3.Distance(position, thumbPosition);
					if(dist < DISTANCE_CLOSE){
						pinchingFingerList.Add(finger.Type);
					}
				} 
			}
		}
		return pinchingFingerList;
	}

	private void updateValidStartFrames () {
		int extended = lastHand.getExtendedFingerCount ();

		if (extended == 0) {
			//We do not want to track frame where pinching is in action
			return;
		}

		if (extendedFingerQueue.Count == frameWindow) {
			int old = extendedFingerQueue.Dequeue ();

			if (old == numFingersStart) {
				validStartFrames--;
			}
		}
		
		extendedFingerQueue.Enqueue (extended);
		if (extended == numFingersStart) {
			validStartFrames++;
		}
	}

	private void updateValidEndFrames () {
		print (pinchFingerQueue);
		int closed = lastPinchingFingers.Count;

		if (pinchFingerQueue.Count == CONFIDENCE_FRAMES) {
			int old = pinchFingerQueue.Dequeue ();

			if (old == numFingersEnd) {
				validEndFrames--;
			}
		}

		pinchFingerQueue.Enqueue (closed);
		if (closed == numFingersEnd) {
			validEndFrames++;
		}
	}

	private void print(Queue<int> q) {
		string s = "";

		foreach(int i in q) {
			s += i + " ";
		}
		Debug.Log (s);
	}
}
