﻿using UnityEngine;
using System.Collections;
using Leap;

public class Converter {

	public static Vector3 LeapToWorldPoint(HandController controller, Vector v) {
		Vector3 point = v.ToUnityScaled();
		return controller.transform.TransformPoint (point);
	}

	public static Vector3 LeapToWorldDirection(HandController controller, Vector v) {
		Vector3 point = v.ToUnity();
		return controller.transform.TransformDirection (point);
	}
}
