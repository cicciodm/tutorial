﻿using UnityEngine;
using System.Collections;

public class ResourceNames {
	public const string MENU_POINTER = "MenuAssets/Pointer";
	public const string MENU_TEXT_MAIN = "MenuAssets/TextMenuMain";
	public const string MENU_TEXT_SUB = "MenuAssets/TextMenuSub";
	public const string MODELS_FOLDER = "Patients/Models/";
	public const string ANATOMY = "Patients/Anatomy/Anatomy";
	public const string SKIN_STANDING = "Patients/Skin/SkinStanding";
	public const string SKIN_DRE = "Patients/DRE/Body/SkinDRE";
	public const string TD_TEXT_WHITE = "TextAssets/3DWhiteText";
	public const string TD_TEXT_BLACK = "TextAssets/3DBlackText";
	public const string BLACK_TEXT_ST = "TextAssets/3DBlackText";
	public const string PROBE = "TouchAssets/FingerProbe";
	public const string SLIDER = "Widgets/Slider";
}
