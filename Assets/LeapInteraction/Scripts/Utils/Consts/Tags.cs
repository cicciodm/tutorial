﻿using UnityEngine;
using System.Collections;

public class Tags {
	public const string MENU_POINTER = "menu_pointer";
	public const string LOADED_MODEL = "loaded_model";
	public const string MENU_ELEM = "menu_elem";
	public const string SKIN_TAG = "skin";
	public const string SKELETON_TAG = "skeleton";
	public const string ANATOMY_TAG = "anatomy";
	public const string BODY_PARTS = "body_parts";
	public const string TEXT_STATE = "text_state";
	public const string TEXT_DESC = "text_description";
	public const string GESTURE_POINTER = "gesture_pointer";
	public const string PROBE = "gesture_pointer";
	public const string TABLE = "table";
	public const string SKIN_STANDING = "skin_standing";
	public const string SKIN_DRE = "skin_dre";
	public const string BINDER = "binder";
	public const string FEMURS = "femurs";
	public const string SLIDER = "slider";
	public const string DRE_SLIDER = "dre_slider";
}
