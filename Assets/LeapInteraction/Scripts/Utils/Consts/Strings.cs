﻿using System.Collections;

public class Strings {
	public const string IDLE_STATE = "Welcome";
	public const string IDLE_DESC = "Use your hand to control the simulation.\nBegin with pointing your index";

	public const string LOADED_STATE = "Model Loaded";
	public const string LOADED_DESC = "Extend 2, 3 or 5 fingers\nto interact with the model";

	public const string POINTING_STATE = "Pointing";
	public const string POINTING_DESC = "Select menus";

	public const string OPEN_STATE = "Rotate Model";
	public const string OPEN_DESC = "Move your hand up/down and left/right to rotate the model";

	public const string MOVE_STATE = "Move model";
	public const string MOVE_DESC = "Move your hand to move the model";

	public const string SWIPE_STATE = "Add/Remove Layer";
	public const string SWIPE_DESC = "Swipe right/left to remove/add model layers";

}
