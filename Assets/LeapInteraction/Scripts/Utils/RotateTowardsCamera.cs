﻿using UnityEngine;
using System.Collections;

public class RotateTowardsCamera : MonoBehaviour {

	public GameObject cameraObject;

	// Use this for initialization
	void Start () {
		gameObject.transform.LookAt (cameraObject.transform.position);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
