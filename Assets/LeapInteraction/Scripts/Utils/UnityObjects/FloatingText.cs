﻿using UnityEngine;
using System.Collections;

public class FloatingText {

	private GameObject parent;
	private GameObject stateObject;
	private GameObject descObject;

	private float verticalDisplacement;
	private float leftHorDisplacement;
	private float rightHorDisplacement;
	
	private int currentHash;
	private int refreshCount;
	private int maxRefresh;

	private Vector3 lookTowards;

	public FloatingText(float verticalDisplacement,
	                    float secondLineDisplacement,
	                    float leftHorDisplacement,
	                    float rightHorDisplacement,
	                    float bigTextScale,
	                    float smallTextScale,
	                    int maxRefresh,
	                    string textPrefabName,
	                    Vector3 lookTowards) {

		this.verticalDisplacement = verticalDisplacement;
		this.leftHorDisplacement = leftHorDisplacement;
		this.rightHorDisplacement = rightHorDisplacement;
		this.lookTowards = lookTowards;
			
		this.maxRefresh = maxRefresh;

		parent = new GameObject ("FloatingText");
		GameObject textPrefab = Resources.Load (textPrefabName) as GameObject;

		stateObject = GameObject.Instantiate (textPrefab);
		stateObject.name = "State";
		stateObject.transform.SetParent (parent.transform);
		stateObject.transform.localScale = new Vector3 (bigTextScale, bigTextScale, bigTextScale);

		descObject = GameObject.Instantiate (textPrefab);
		descObject.name = "Desc";
		descObject.transform.SetParent (parent.transform);
		descObject.transform.position = new Vector3 (0f, secondLineDisplacement, 0f);
		descObject.transform.localScale = new Vector3 (smallTextScale, smallTextScale, smallTextScale);
	
		resetText ();
	}



	public void updateFloatingText(Vector3 fingerTip, int fingerID, string state, string desc, bool isLeftHand) {

		state = state == null ? "" : state;
		desc = desc == null ? "" : desc;

		int hash = getFingerGestureHash(fingerID, state);
		parent.transform.position = calculateTextPosition (fingerTip, isLeftHand);
		parent.transform.LookAt (lookTowards);

		if (hash != currentHash) {

			TextMesh stateMesh = stateObject.GetComponentInChildren<TextMesh> ();
			TextMesh descMesh = descObject.GetComponentInChildren<TextMesh> ();

			TextAlignment alignment = isLeftHand ? TextAlignment.Left : TextAlignment.Right;
			TextAnchor anchor = isLeftHand ? TextAnchor.UpperLeft : TextAnchor.UpperRight;


			setTextAndProperties (stateMesh, state, alignment, anchor);
			setTextAndProperties (descMesh, desc, alignment, anchor);
			currentHash = hash;
			refreshCount = 0;
		} else {
			if (refreshCount >= maxRefresh) {
				resetText();
			} else {
				refreshCount ++;
			}
		}
		
	}

	private void resetText() {
		setObjectText (stateObject, "");
		setObjectText (descObject, "");
	}

	public void resetFloatingText() {
		resetText ();
		currentHash = 0;
		refreshCount = 0;
	}

	private Vector3 calculateTextPosition(Vector3 position, bool isLeftHand) {
		float yVal = position.y + verticalDisplacement;
		float zVal = position.z;
		float xVal = isLeftHand ? position.x + leftHorDisplacement : position.x + rightHorDisplacement;

		return new Vector3 (xVal, yVal, zVal);
	}

	private void setTextAndProperties(TextMesh mesh, string str, TextAlignment alignment, TextAnchor anchor) {
		mesh.alignment = alignment;
		mesh.anchor = anchor;
		mesh.text = str;
	}
	
	private void setObjectText(GameObject obj, string text) {
		TextMesh tMesh = obj.GetComponentInChildren<TextMesh> ();
		tMesh.text = text;
	}

	private void setMeshText(TextMesh mesh, string text) {
		mesh.text = text; 
	}

	private int getFingerGestureHash(int fingerID, string state) {
		int hash = 17;
		hash = hash * 31 + fingerID.GetHashCode ();
		hash = hash * 31 + state.GetHashCode ();
		return hash;
	}

	public class FloatingTextBuilder {

		private float verticalDisplacement;
		private float secondLineDisplacement;
		private float leftHorDisplacement;
		private float rightHorDisplacement;
		
		private float bigTextScale;
		private float smallTextScale;
		
		private int maxRefresh;

		private string textPrefabName;

		private Vector3 lookTowards;

		public FloatingTextBuilder() {
			verticalDisplacement = 0.3f;
			secondLineDisplacement = -0.45f;
			leftHorDisplacement = 0.3f;
			rightHorDisplacement = -0.3f;
			
			bigTextScale = 0.3f;
			smallTextScale = 0.15f;
			
			maxRefresh = 120;

			textPrefabName = ResourceNames.TD_TEXT_BLACK;

			lookTowards = 5000 * new Vector3(0, 0, -1);
		}

		public FloatingTextBuilder withVerticalDisplacement(float val) {
			this.verticalDisplacement = val;
			return this;
		}

		public FloatingTextBuilder withSecondLineDisplacement(float val) {
			this.secondLineDisplacement = val;
			return this;
		}

		public FloatingTextBuilder withLeftHorDisplacement(float val) {
			this.leftHorDisplacement = val;
			return this;
		}

		public FloatingTextBuilder withRightHorDisplacement(float val) {
			this.rightHorDisplacement = val;
			return this;
		}

		public FloatingTextBuilder withBigTextScale(float val) {
			this.bigTextScale = val;
			return this;
		}

		public FloatingTextBuilder withSmallTextScale(float val) {
			this.smallTextScale = val;
			return this;
		}

		public FloatingTextBuilder withMaxRefreshValue(int val) {
			this.maxRefresh = val;
			return this;
		}

		public FloatingTextBuilder withTextPrefabName(string name) {
			this.textPrefabName = name;
			return this;
		}

		public FloatingTextBuilder lookingTowards(Vector3 point) {
			this.lookTowards = point;
			return this;
		}

		public FloatingText buildFloatingText() {
			return new FloatingText (verticalDisplacement,
			                         secondLineDisplacement,
			                         leftHorDisplacement,
			                         rightHorDisplacement,
			                         bigTextScale,
			                         smallTextScale,
			                         maxRefresh,
			                         textPrefabName,
			                         lookTowards);
		}



	}

}
