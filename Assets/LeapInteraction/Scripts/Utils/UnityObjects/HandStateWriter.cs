﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HandStateWriter {

	private static HandStateWriter instance = null;
	private TextMesh state;
	private TextMesh desc;

	private HandStateWriter() {
		state = GameObject.FindGameObjectWithTag (Tags.TEXT_STATE).GetComponent<TextMesh>();
		desc = GameObject.FindGameObjectWithTag (Tags.TEXT_DESC).GetComponent<TextMesh>();

		resetText ();
	}

	public static HandStateWriter getInstance() {
		if (instance == null) {
			instance = new HandStateWriter();
		}
		return instance;
	}

	public void writeStateAndDescription(string state, string desc) {
		writeState(state);
		writeDescription(desc);
	}

	public void writeState(string stateString) {
		write (stateString, state);
	}

	public void writeDescription(string descString) {
		write (descString, desc);
	}

	public void resetText() {
		write (Strings.IDLE_STATE, state);
		write (Strings.IDLE_DESC, desc);
	}

	private void write(string s, TextMesh t) {
		t.text = s;
	}


}
