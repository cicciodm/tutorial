﻿using UnityEngine;
using System.Collections;

public class SettableTransform {

	private Vector3? position = null;
	private Quaternion? rotation = null;
	private Vector3? scale = null;

	public SettableTransform() {

	}

	public SettableTransform(Vector3 pos, Quaternion rot, Vector3 sc) {
		position = pos;
		rotation = rot;
		this.scale = sc;
	}

	public void setPosition(Vector3 pos) {
		position = pos;
	}

	public void setScale(Vector3 sc) {
		scale = sc;
	}

	public void setEulerAngles(Vector3 angles) {
		Quaternion rot = new Quaternion ();
		rot.eulerAngles = angles;
		rotation = rot;
	}

	public Vector3 getEulerAngles() {
		return rotation.Value.eulerAngles;
	}

	public Vector3 getPosition() {
		return position.Value;
	}

	public void attachToGameObject(GameObject obj) {
		if(position != null) obj.transform.position = position.Value;
		if(rotation != null) obj.transform.rotation = rotation.Value;
		if(scale != null) obj.transform.localScale = scale.Value;
	}
}
